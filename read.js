var fs = require('fs'), readline = require('readline');

var rd = readline.createInterface({
    input: fs.createReadStream('test.conf'),
    output: process.stdout,
    terminal: false
});

rd.on('line', function(line) {
    var method = line.split(/:/);
    switch (method[0].trim()) {
        case "INSTALL" :
            console.log('Installing ' + method[1].trim())
        case "UNINSTALL" :
            console.log('Removing ' + method[1].trim())
        case "LINK" :
            console.log('LINK ' + method[1].trim())
        default :
            console.log('unknown')
    } 
});